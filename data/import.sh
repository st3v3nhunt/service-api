#!/bin/bash

ruby data/create_fake_services.rb

tail -n +2 data/services.csv |
  awk -F "," '{ 
split($18,o,"|"); op="\""; for(i=1;i<length(o);i++){ op = op"\",\""o[i] } op=op"\"";
split($19,g,"|"); ge="\""; for(i=1;i<length(g);i++){ ge = ge"\",\""g[i] } ge=ge"\"";
split($20,s,"|"); se="\""; for(i=1;i<length(s);i++){ se = se"\",\""s[i] } se=se"\"";
split($21,l,"|"); la="\""; for(i=1;i<length(l);i++){ la = la"\",\""l[i] } la=la"\"";
sub(/\",\"/, "", op);
sub(/\",\"/, "", ge);
sub(/\",\"/, "", se);
sub(/\",\"/, "", la);
print "{ \"id\" : "$1", \"name\" : \""$2"\", \"org_type\" : \""$3"\", \"number_of_patients\" : "$12", \"accepting_new_patients\" : "$13", \"pims_url\" : \""$16"\", \"online_perscription_ordering\" : "$17",\"loc\" : { \"type\" : \"Point\", \"coordinates\" : [ "$11", "$10" ] }, \"address\" : { \"line_1\" : \""$4"\", \"line_2\" : \""$5"\", \"line_3\" : \""$6"\", \"city\" : \""$7"\", \"county\" : \""$8"\", \"postcode\": \""$9"\" }, \"phone\" : \""$14"\", \"email\" : \""$15"\", \"services\" : [ "se" ], \"opens\" : [ "op" ], \"genders\" : [ "ge" ], \"languages\" : [ "la" ] }" }' > data/services.json

mongoimport --host localhost --db services --collection services --file data/services.json --drop
