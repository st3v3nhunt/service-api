require 'csv'
require 'faker'

def headers
  [:id, :name, :org, :line_1, :line_2, :line_3, :city,
   :county, :postcode, :lat, :long, :patients,
   :accepting, :phone, :email, :url, :eps,
   :early, :late, :weekends, :male, :female,
   :services,
   :languages]
end

def name
  Faker::Company.name.gsub(',','')
end

def org
  [:GBP, :HOS, :DEN].sample
end

def line_1
  Faker::Address.secondary_address
end

def line_2
  Faker::Address.street_name
end

def line_3
  Faker::Address.street_suffix
end

def city
  Faker::Address.city
end

def county
end

def postcode
  Faker::Address.zip_code
end

def long
  (-923..269).to_a.sample / 100.0 
end

def lat
 (4984..6085).to_a.sample / 100.0 
end

def patients
  (1..200).to_a.sample
end

def accepting
  random_bool
end

def phone
  Faker::PhoneNumber.phone_number
end

def email
  Faker::Internet.email
end

def url
  Faker::Internet.url
end

def eps
  random_bool
end

def opens
  [:early, :late, :weekend].sample(rand(2..3)).join '|' 
end

def genders
  [:male, :female].sample(rand(1..2)).join '|'
end

def languages
  [:english, :french, :polish, :german, :spanish].sample(rand(3..5)).join '|'
end

def services
  [:asthma, :diabetes, :eye_test, :ent, :walk_in, :smoking_cessation]
  .sample(rand(3..6)).join '|'
end

def random_bool
  [true, false].sample
end

CSV.open( "#{Dir.pwd}/data/services.csv", 'w' ) do |csv|
  csv << headers
  (1..1000).each do |x|
    csv << [x, name, org, line_1, line_2, line_3, city, county, postcode, lat,
            long, patients, accepting, phone, email, url, eps, opens, genders,
            services, languages]
  end 
end
