require 'sinatra'
require 'json'
require 'mongo'

include Mongo

def host
  p ENV['MONGO_RUBY_DRIVER_HOST'] || 'localhost'
end

def port
  p ENV['MONGO_RUBY_DRIVER_PORT'] || MongoClient::DEFAULT_PORT
end

def non_heroku_connection
  p 'non_heroku_connection'
  MongoClient.new(host, port).db('services')
end

def heroku_connection
  p 'heroku connection'
  p host = ENV['MONGOLAB_URI'][/@(.*):/, 1]
  p port = ENV['MONGOLAB_URI'][/@.*:(\d+)\//, 1]
  p user = ENV['MONGOLAB_URI'][/\/\/(\w+):/, 1]
  p password = ENV['MONGOLAB_URI'][/:(\w+)@/, 1]
  db = MongoClient.new(host, port).db(user)
  db.authenticate(user, password)
end

configure do
  set :services_db, ENV['MONGOLAB_URI'].nil? ? non_heroku_connection : heroku_connection
end


get '/' do
  content_type :json
  { :data => 
    settings
    .services_db['services']
    .find( {}, :fields => { :_id => 0 } )
    .to_a
  }.to_json
end

# example url /services?lat=50&lon=-3&limit=10&offset=10&service=asthma&gender=male&language=english
# all services
get '/services/?' do
  content_type :json

  # build filters out into hash # TODO: DRY this out
  filters = { :facilities => { '$in' => params[:facility] ? params[:facility].downcase.split(',') : [] },
              :genders    => { '$in' => params[:gender]   ? params[:gender].downcase.split(',')   : [] },
              :languages  => { '$in' => params[:language] ? params[:language].downcase.split(',') : [] },
              :opens      => { '$in' => params[:open]     ? params[:open].downcase.split(',')     : [] },
              :org_type   => { '$in' => params[:org]      ? params[:org].downcase.split(',')      : [] },
              :services   => { '$in' => params[:service]  ? params[:service].downcase.split(',')  : [] }
  }.reject { |k, v| v['$in'].empty? }

  data = settings
  .services_db['services']
  .find(
    { :loc => { 
      :$near => { 
        :$geometry => { 
          :type => 'Point',
          :coordinates => [ params[:lon].to_f , params[:lat].to_f ]
        }
      }
    }
    }.merge(filters),
    { :fields => { :_id => 0 },
      :skip   => params[:offset].to_i || 0,
      :limit  => params[:limit].to_i  || 100
    }).to_a

    status data.empty? ? 404 : 200

    { :data => data }.to_json
end

get '/services/?:id?' do |id|
  content_type :json
  settings
  .services_db['services']
  .find_one( { :id => id.to_i },
            { :fields => { :_id => 0 }} )
  .to_json
end
