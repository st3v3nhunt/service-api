# Service API

## Set it up

Install [MongoDB](http://docs.mongodb.org/manual/)

Generate some data:

    .\data\import.sh

Create index:

    db.services.ensureIndex( { loc : "2dsphere", name : 1, org_type : 1, number_of_patients : 1, accepting_new_patients : 1, pims_url : 1, online_perscription_ordering : 1, address : 1, phone : 1, email : 1, services : 1, opens : 1, genders : 1, languages : 1 }, { name : "service_index" } )

Run the application:

    rackup


